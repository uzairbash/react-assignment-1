import React from 'react';
import './App.css';
import Jobs from "./jobs.json";

const demo = props => {
    return(
     <div className= "job"> 
           <div> 
                
                <h1>Exercise 1: understanding JSX</h1>
                <p>You need to convert this html code into JSX.</p>
          </div>
    
          <div>
                <h2>MentorStudents Job-Portal</h2>
                <img src="https://picsum.photos/id/180/500/200" style={{ }} ></img><br/><br/>
                <label for="what">What</label>
                <input type="text" id="what"placeholder="Job title,keywords or company" ></input>&nbsp;&nbsp;
                &nbsp;&nbsp;
                <label for="where">Where</label>
                <input type="text" id="where" placeholder=" Enter city"></input>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="submit" style={{color:"blue"}}>Find jobs</button>
         </div>
    </div>
       
    );
};

// ReactDOM.render(
//     <React.StrictMode>
//       <App />
//     </React.StrictMode>,
//     document.getElementById('root')
//   );
 export default demo;
