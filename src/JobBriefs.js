import React from 'react';

const JobBrief = props =>{
    
    return(


        
        <div style={{maxWidth:"50vw",border:"2px solid black", margin: "0 auto"}}>
            <h1>{props.name}</h1>
            <h3>
                <span>{props.city}</span>,&nbsp;
                <span>{props.country}</span>
            </h3>
            <img src="https://picsum.photos/id/180/200/100"alt="image"style={{display:"block", marginLeft:"65px"}} />
            <p>{props.description}</p>
            <p>Salary:{props.salary}</p>
            
        </div>


    );

    
};
export default JobBrief;
