import React from 'react';
import './App.css';
import Jobs from "./jobs.json";

const jobsprof = props => {
    return(
        <div style={{maxWidth:"20vw", marginLeft:"500px",   }} >
        <div>
           <h4>Full-stack developer</h4>
            <h5>Delhi</h5>
            <img src="https://picsum.photos/id/180/200/100"  ></img>
            <p>
              Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
              Quick decision makers, living with the passion of work, never feeling or saying "I am tired"
            </p>
            <div className="salary">Salary: 30K - 40K</div>
            <button style={{backgroundColor:"blue", color:"white"}}>Apply</button>&nbsp;&nbsp;
            <button style={{backgroundColor:"red", color:"white"}}>Not interested</button>
            <hr/>
        </div>
      
        <div>
            <h4>Front-end developer</h4>
            <h5>Pune</h5>
            <img src="https://picsum.photos/id/180/200/100" className="job-post-image"/>
            <p>
              Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
              Quick decision makers, living with the passion of work, never feeling or saying "I am tired"
            </p>
            <div class="salary">Salary: 15K - 30K</div>
            <button style={{backgroundColor:"blue", color:"white"}}>Apply</button>&nbsp;&nbsp;
            <button style={{backgroundColor:"red", color:"white"}}>Not interested</button>
            <hr/>
        </div>
      
          <div>
            <h4>Back-end developer</h4>
            <h5>Mumbai</h5>
            <img src="https://picsum.photos/id/180/200/100" className="job-post-image"/>
            <p>
              Company looking for some crazy people, we are looking for challenge accepter who move with "I can do" attitude.
              Quick decision makers, living with the passion of work, never feeling or saying "I am tired"
            </p>
            <div class="salary">Salary: 20K - 30K</div>
            <button  style={{backgroundColor:"blue", color:"white"}}>Apply</button>&nbsp;&nbsp;
            <button style={{backgroundColor:"red", color:"white"}}>Not interested</button>
            <hr/>
          </div>
          </div>
      
       
    );
};

// ReactDOM.render(
//     <React.StrictMode>
//       <App />
//     </React.StrictMode>,
//     document.getElementById('root')
//   );
 export default jobsprof;