import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Jobs from "./jobs.json";
import Demo from "./Demo";
import JobsProf from "./JobProf";
import JobBriefs from './JobBriefs';


function App() {
  return (
    <div className="App">
     
      <div>
            <Demo/> 
      </div>

      <div>
            <JobsProf/>
      </div>

      <div>
            <JobBriefs
              name="Full-stack developer"
              city="Delhi" 
              country="India"
              description="Exprience and understsnding of MERN stack (MongoDB, Express, React js, Node js) or similar frameworks like Angular js, Vue js, D3 js etc, will be appriciated "
              salary="65k"
            />
            <br></br>
             <JobBriefs
              name="HR"
              city="Chandigarh" 
              country="India"
              description="HRIS Administration responsibilities include managing our internal databases, keeping employee records in digital format and educating users on how to use our HR systems. To be successful in this role, you should have exprience  with database administration and human resources management software like payroll or applicant tracking systems."
              salary="25k"
            />
            <br></br>

            <JobBriefs
              name="Back-end devloper"
              city="Pune" 
              country="India"
              description="We are looking for an excellant experienced person in backend developer field. Be a part of a vibrant, rapidly growing tech enterprise with a great working environment. As an backend developer you will be responsible for the server side of our web application anf will work closely with our engineer to ensure the system consistancy and improve your experience.  "
              salary="40k"
            />
            <br></br>

            <JobBriefs
              name="Front-end devloper"
              city="Chandigarh" 
              country="India"
              description="We are looking for a talented user experience designer to create amazing user experience. The ideal candidate should have an eye for  clean and artful design, possess superior  UI skills and be able to  translate high level requirements into interaction flows and artful, and trandform them into beautiful, intuitive and functional user interfaces. Expert understanding of cintemporary user- centered disgn methodologies for web and mobile is a must. "
              salary="19k"
            />

              <br></br>

              <JobBriefs
                name="SEO"
                city="Noida" 
                country="India"
                description=" Experienced in google ad-works, SEO, SMO(on page and off page )PPC, Digital marketing cmpaigns, Marketing database, social media and display advertising campaings. "
                salary="15k"
              />

      </div>
        
    </div>
    
  );
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


export default App;
